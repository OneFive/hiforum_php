<?php

class Model_Thread extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'name',
		'author',
		'section',
		'rating',
		'views',
		'attached',
		'closed',
		'answer',
		'created_at',
		'updated_at'
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);

    public static function htmlEditorValid($html) {

        $allowable_tags = array(
            'tr' => '',
            'td' => '',
            'th' => '',
            'b' => '',
            'i' => '',
            'u' => '',
            'strong' => '',
            'thead' => '',
            'tbody' => '',
            'br' => '',
            'p' => '',
            'ol' => '',
            'li' => '',
            'ul' => '',
            'h1' => '',
            'h2' => '',
            'span' => ['class' => "*"],
            'table' => ['class' => "table table-bordered"],
            'a' => ['href' => '*', 'rel' => "nofollow", 'target' => "_blank"],
            'img' => ['src' => '*', 'alt' => ''],
            'div' => ['class' => "alert"]
        );

        $html = strip_tags($html, '<'. implode(array_keys($allowable_tags), '><') .'>');

        preg_match_all('/<[ ]*([a-z0-9]+?)[ ]*(>|(.*)>)/Uis', $html, $tags, PREG_SET_ORDER);

        foreach($tags as $tag) {
            $attrs = $tag[0];
            $tag_name = $tag[1];

            preg_match_all('/([a-z]+?)[ \r\n\t]*=[ \r\n\t]*([\'](.*)[\']|["](.*)["])/Uis', $attrs, $ats, PREG_SET_ORDER);

            foreach($ats as $attr) {
                $val  = trim($attr[2]);
                $attr_name = trim($attr[1]);
                
                $allow = isset($allowable_tags[$tag_name]) ? $allowable_tags[$tag_name] : '';

                if(is_array($allow)) {
                    if(isset($allow[$attr_name])) {
                        if($val != $allow[$attr_name] && $allow[$attr_name][0] != '*') {
                            $attrs = str_replace($val, $allow[$attr_name], $attrs);
                        }
                        // javascript://
                        if(($attr_name == 'href' || $attr_name == 'src') && ($val[0] != 'j' || $val[0] == 'J')) {
                            $attrs = str_replace($val, 'http://'.$val, $attrs);
                        }
                    } else {
                        $attrs = str_replace($attr[0], '', $attrs);
                    }
                } else {
                    if($allow == '') {
                        $attrs = str_replace($attr[0], '', $attrs);
                    }
                }
            }

            $html = str_replace($tag[0], $attrs, $html);
        }

       return $html;

    }

}
