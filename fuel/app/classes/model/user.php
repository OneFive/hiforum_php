<?php

class Model_User extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'login',
		'name',
		'family',
		'answers',
		'rating',
		'plus',
		'minus',
		'info',
		'sign',
		'birthday',
		'email',
		'site',
		'icq',
		'themes',
		'last_msg',
		'u_schemes',
		'u_companents',
		'p_schemes',
		'p_companents',
		'reg_blog',
		'ip_auth',
		'status',
		'ban',
		'bans',
		'avatar',
		'jabber',
		'p_email',
		'gold',
		'password',
		'token',
        'token_time',
		'location',
		'city',
		'country',
		'language',
		'style',
		'created_at',
		'updated_at'
	);

    public static function new_token($email, $hash) {
        $token = md5(str_replace( ['f', '4', 'a', 'c'], ['8', '5', '9', '0'],md5($email. sha1(md5($hash . '!3D2sd4$%"\\08'. $email)))));
        return md5($hash.rand(111111,999999).$token.mt_rand(1111111,9999999));
    }

    public static function crypt_pass($email, $password) {
        return md5(str_replace( ['a', '8', 'f', '4'], ['0', 'c', '6', '7'],md5($email. sha1(md5($password . '!3D2sd4$%"\\08'. $email)))));
    }

    public static function chech_pass($hash, $email) {
        return Model_User::query()->where('email', $email)->and_where_open()->where('password', $hash)->and_where_close()->get_one();
    }

    public static function auth($password, $email) {
        $hash = Model_User::crypt_pass($email, $password);
        $user = Model_User::chech_pass($hash, $email);
        if($user) {
            $token = Model_User::new_token($email, $hash);
            $user->set('token', $token);
            $user->save();
            //setcookie("token", $token, time() + 2592000);
            Cookie::set('token', $token, 2592000);

            $user = $user->to_Array();
            unset($user['password']);

            return $user;
        }
    }

    public static function registration($login, $email, $password) {

        $val = Validation::forge();

        $val->add_callable('rules')
            ->add_callable(new rules());

        $val->add_field('login', '', 'required', array('trim', 'strip_tags', 'required', 'is_lower'))
            ->add_rule('min_length', 5)
            ->add_rule('max_length', 20)
            ->add_rule('unique', 'users.login')
            ->add_field('email', '', 'required')
            ->add_rule('valid_email')
            ->add_rule('unique', 'users.email')
            ->add_field('password', '', 'required')
            ->add_rule('min_length', 4);

        if ($val->run())
        {
            $hash = Model_User::crypt_pass($email, $password);
            $token = Model_User::new_token($email, $hash);

            $user = new Model_User();
            $user->login = $login;
            $user->password = $hash;
            $user->email = $email;
            $user->save();
            $user->set('token', $token);
            $user->save();

            //setcookie("token", $token, 2592000 + time());
            Cookie::set('token', $token, 2592000);

            $user = $user->to_Array();
            unset($user['password']);

            return $user;
        }
        else
        {
            $errors = $val->error();
            foreach($errors as &$v) {
                unset($v->params, $v->value);
            }

            return $errors;
        }

    }

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);
}
