<?php

class Model_Answer extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'author',
		'thread',
		'notice',
		'notice_type',
		'notice_author',
		'message',
		'created_at',
		'updated_at',
        'deleted'
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);
}
