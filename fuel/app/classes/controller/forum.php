<?php

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 * 
 * @package  app
 * @extends  Controller
 */
class Controller_Forum extends Controller_Rest//Controller
{
    private $user;
	/**
	 * The basic welcome message
	 * 
	 * @access  public
	 * @return  Response
	 */
	public function before()
	{
        parent::before();

        Package::load('orm');

        if(Input::cookie('token'))
            $this->user = Model_User::query()->where('token', Input::cookie('token'))->get_one();
    }
    //*/
		//$this->view = View::forge('layout');
		///$this->theme = \Theme::instance();

        // set the page template
        ///$this->theme->set_template('layout');

        // set the page title (can be chained to set_template() too)
        ///$this->theme->get_template()->set('title', 'My homepage');

        // set the homepage navigation menu
        //$this->theme->set_partial('navbar', 'homepage/navbar');

        // define chrome with rounded window borders for the sidebar section
        //$this->theme->set_chrome('sidebar', 'chrome/borders/rounded', 'partial');

        // set the partials for the homepage sidebar content
        //$this->theme->set_partial('sidebar', 'homepage/widgets/login');
        //$this->theme->set_partial('sidebar', 'homepage/widgets/news')->set('users', Model_News::latest(5));

        // call the user model to get the list of logged in users, pass that to the users sidebar partial
       // $this->theme->set_partial('sidebar', 'homepage/widgets/users')->set('users', Model_User::logged_in_users());
	//}

	//public function after($response)
    //{
        // If no response object was returned by the action,
        //if (empty($response) or  ! $response instanceof Response)
        //{
            // render the defined template
         //   $response = \Response::forge(\Theme::instance()->render());
        //}//

        //return parent::after($response);
    //}

    public function get_sections()
    {
        $query = 'SELECT count(DISTINCT `t`.`id`) AS `count`, count(DISTINCT `a`.`id`) AS `answers`, `s`.`name`,`s`.`id`,`s`.`parent`,`s`.`description`,`s`.`icon`,MAX(`a`.`created_at`) AS `last`,MAX(CASE WHEN `a`.`created_at` THEN `a`.`author` ELSE 0 END) AS `user`  FROM `threads` AS `t` RIGHT JOIN `answers` AS `a` ON `t`.`id` = `a`.`thread` RIGHT JOIN `sections` AS `s` ON `s`.`id` = `t`.`section` GROUP BY `s`.`id` ORDER BY `s`.`sort` ASC';
        $sections = DB::query($query, DB::SELECT)->execute()->as_array();

        return $this->response($sections);
    }

    public function get_section()//action_sections()
    {
        $rows = 20;
        $page = (int)$this->param('page');
        $page = $page < 1 ? 1 : $page;
        $start = $page * $rows - $rows;
        $stop = $page * $rows;

        $section = (int)$this->param('id');

        $query = 'SELECT
            count(DISTINCT `a`.`id`) AS `answers`,
            `t`.`name`,
            `t`.`id`,
            `t`.`author`,
            `t`.`attached`,
            `t`.`section`,
            `t`.`rating`,
            `t`.`views`,
            `t`.`updated_at`,
            `t`.`created_at`,
            `t`.`closed`,
            `u`.`login` AS `username`,
            MAX(`a`.`created_at`) AS `last`
            FROM `threads` AS `t`
            RIGHT JOIN `answers` AS `a` ON `t`.`id` = `a`.`thread`
            LEFT JOIN `users` AS `u` ON `u`.`id` = `t`.`author`
            WHERE `t`.section = '.$section.'
            GROUP BY `t`.`id`
            ORDER BY
                IF(`t`.attached = 1, 9999999999 , MAX(`a`.`created_at`)) DESC
            LIMIT '. $start .','. $stop;
        $threads = DB::query($query, DB::SELECT)->execute()->as_array();


$query = 'SELECT COUNT(`s`.`id`) as `count` , `s`.`name`, `s`.`id`
	FROM `sections` as `s`
	RIGHT JOIN `threads` as `t` ON `s`.`id` = `t`.`section`
		WHERE `s`.`id` = '. $section;

        $info = DB::query($query, DB::SELECT)->execute()->as_array()[0];


        $pages = ceil($info['count'] / $rows);

        $this->response(array(
            'info' => $info,
            'pages' => $pages,
            'threads' => $threads
        ));
    }

    public function get_last()
    {
        $query = 'SELECT
            count(DISTINCT `a`.`id`) AS `answers`,
            `t`.`name`,
            `t`.`id`,
            `t`.`author`,
            `t`.`section`,
            `t`.`rating`,
            `t`.`views`,
            `t`.`updated_at`,
            `t`.`created_at`,
            `t`.`closed`,
            `u`.`login` AS `username`,
            MAX(`a`.`created_at`) AS `last`
            FROM `threads` AS `t`
            RIGHT JOIN `answers` AS `a` ON `t`.`id` = `a`.`thread`
            LEFT JOIN `users` AS `u` ON `u`.`id` = `t`.`author`
            GROUP BY `t`.`id` ORDER BY `a`.`created_at` DESC LIMIT 30;';

        $threads = DB::query($query, DB::SELECT)->execute()->as_array();

        $this->response(array(
            'pages' => 1,
            'threads' => $threads
        ));

    }

    public function get_thread()
    {
        $rows = 20;
        $page = (int)$this->param('page');
        $page = $page < 1 ? 1 : $page;
        $start = $page * $rows - $rows;
        $stop = $page * $rows;

        $thread = (int)$this->param('id');

        $query = 'SELECT `a`.id, `a`.message, `a`.author, `u`.rating, `t`.name, `t`.views,`t`.attached, `t`.closed, `a`.created_at, `a`.updated_at, `a`.notice, `a`.notice_type, `t`.section, `u`.login as `username`, `u`.avatar FROM `threads` AS `t` RIGHT JOIN `answers` AS `a` ON `a`.`thread` = `t`.`id` LEFT JOIN `users` AS `u` ON `u`.`id` = `a`.`author` WHERE `t`.`id` = '.$thread. ' AND `a`.deleted = 0 LIMIT '.$start.','.$stop;
        $answers = DB::query($query, DB::SELECT)->execute()->as_array();

        $query = 'UPDATE `threads` SET `views` = IFNULL(`views` + 1, 1) WHERE `id` = '. $thread;
        DB::query($query, DB::UPDATE)->execute();


        $query = 'SELECT COUNT(`a`.`id`) as `count` , `t`.`closed`, `t`.`name`, `t`.`id`, `s`.`name` as `section_name`, `s`.`id` as `section_id`,`t`.`attached`
	       FROM `threads` as `t`
	       RIGHT JOIN `sections` as `s` ON `s`.`id` = `t`.`section`
	       RIGHT JOIN `answers` as `a` ON `t`.`id` = `a`.`thread`
		   WHERE `t`.`id` = '. $thread;

        $info = DB::query($query, DB::SELECT)->execute()->as_array()[0];


        $pages = ceil($info['count'] / $rows);
        $this->response(array(
            'info' => $info,
            'pages' => $pages,
            'answers' => $answers
        ));

    }

    public function put_thread()
    {
        if($this->user && $this->user->status > 1) {
            $query = DB::update('threads')
            ->value('attached', Input::get('attached'))
            ->value('closed', Input::get('closed'))
            ->value('name', Input::get('name'))
            ->value('section', Input::get('section_id'))
            ->where('id', $this->param('id'))->execute();
            $this->response('ok');
        } else
            $this->response('permissions', 403);
    }

    public function post_thread()
    {

        $message = Input::post('message');
        $title   = Input::post('title');

        $message = Model_Thread::htmlEditorValid($message);

        $new = new Model_Thread();
        $new->name = $title;
        $new->section = Input::post('section');
        $new->author = $this->user ? $this->user->id : 0;
        $new->save();

        $new1 = new Model_Answer();
        $new1->thread = $new->id;
        $new1->message = $message;
        $new1->author = $new->author;
        $new1->save();

        $this->response(array('id' => $new->id));
    }

    public function post_answer()
    {

        $message     =  Input::post('message');
        $thread_id   =  Input::post('thread');
        $answer_id   =  Input::get('answer_id');
        $notice      =  Input::post('notice');
        $notice_type =  Input::post('notice_type');

                $thread = Model_Thread::query()->where('id', $thread_id)->get_one();
    
                if(!$thread->closed || $this->user->status > 1) {

                    // new answer
                    if($answer_id == 0) {
    
                        $message = Model_Thread::htmlEditorValid($message);
        
                        $new1 = new Model_Answer();
                        $new1->thread = $thread_id;
                        $new1->message = $message;
                        $new1->author = $this->user ? $this->user->id : 0;
                        $new1->save();
                    } else {//edit answer
                        if($this->user) {
                            $query = DB::update('answers')
                                ->value('message', $message);

                            $query->and_where_open()
                                ->where('id', $answer_id);

                            // admin
                            if($this->user->status < 2)
                                    $query->and_where('author', $this->user->id);


                            $query->and_where_close()->execute();
                            //if($query->and_where_close()->execute()) {
                                $this->response('ok');
                            //}
                            
                        } else {
                            $this->response('permissions');
                        }
                    }


                }

        //$this->response(array());
    }

    public function put_answer()
    {
        $answer_id   =  Input::get('id');
        $notice      =  Input::get('notice');
        $notice_type =  Input::get('notice_type');


        if($this->user && $this->user->status > 1) {
                    $query = DB::update('answers')
                        ->value('notice', $notice)
                        ->value('notice_type', $notice_type)
                        ->where('id', $answer_id)
                    ->execute();
                    //if($query->and_where_close()->execute()) {
                    $this->response('ok');
                    //}

        }else {
                $this->response('permissions');
        }
        //$this->response(array());
    }

    public function delete_answer()
    {
        $answer_id   =  Input::get('id');


        if($this->user && $this->user->status > 4) {
            $query = DB::update('answers')
                ->value('deleted', 1)
                ->where('id', $answer_id)
                ->execute();
            //if($query->and_where_close()->execute()) {
            $this->response('ok');
            //}

        }else {
            $this->response('permissions');
        }
        //$this->response(array());
    }

    public function get_profile() {
        $username = $this->param('username');
        $user = Model_User::query()->where('login',  $username)->get_one();
        if($user) {
            $user = $user->to_array();
            unset($user['password'], $user['token'], $user['email'], $user['ip_auth']);
        }

        $this->response($user);

    }

    public function post_profile() {

        $this->response('test');

    }


    public function post_auth()
    {

        $this->response(
            Model_User::auth(  Input::post('password'), Input::post('email') )
        );
    }

    public function post_registration()
    {

        $this->response(
            Model_User::registration( Input::post('login'), Input::post('email'), Input::post('password') )
        );
    }


    public function action_index()
    {
        $data = array('user' => '');

        if( $this->user) {
            $user =  $this->user->to_array();
            unset($user['password']);
            $data['user'] = $user;
        }

        return Response::forge(View::forge('layout', $data));
    }





	/**
	 * The 404 action for the application.
	 * 
	 * @access  public
	 * @return  Response
	 */
	public function action_404()
	{
		return Response::forge(ViewModel::forge('welcome/404'), 404);
	}
}
