<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html" xmlns:ng="http://angularjs.org" xmlns="http://www.w3.org/1999/html">
<head>
	<meta charset="utf-8">
	<title></title>
    <?php //echo Asset::css('bootstrap.min.css.gz'); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php
        $theme = substr(Input::get('t'),0, 5);
        $theme = '';
        if($theme) {
            echo '<link type="text/css" id="theme_css" rel="stylesheet" href="/assets/css/themes/bootstrap.'.$theme.'.min.css" />';
        } else echo '<link type="text/css" id="theme_css" rel="stylesheet" href="/assets/css/bootstrap.min.css.gz" />';
    ?>
    <link type="text/css" rel="stylesheet" href="/assets/css/bootstrap-responsive.min.css.gz" />
    <link type="text/css" rel="stylesheet" href="/assets/css/datepicker.css" />
    <link id="favicon" rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet/less" type="text/css" href="/assets/less/styles.less">
    <link rel="stylesheet/less" type="text/css" href="/assets/css/select2.css">


    <!-- Todo: js join/gzip/min-->
    <script type="text/javascript" src="/assets/js/jquery-1.8.3.min.js.gz"></script>
    <script type="text/javascript" src="/assets/js/bootstrap.min.js.gz"></script>
    <script type="text/javascript" src="/assets/js/angular106_and_cookie.min.js.gz"></script>
    <script type="text/javascript" src="/assets/js/angular-locale_ru-ru.js"></script>
    <script type="text/javascript" src="/assets/js/resource.min.js"></script>
    <script type="text/javascript" src="/assets/js/jquery.easing.1.3.min.js.gz"></script>
    <script type="text/javascript" src="/assets/js/jquery.blockUI.js"></script>
    <script type="text/javascript" src="/assets/js/bootstrap-datepicker.min.js.gz"></script>
    <script type="text/javascript" src="/assets/js/bootstrap-tab.js"></script>
    <script type="text/javascript" src="/assets/js/less-1.3.3.min.js"></script>
    <script type="text/javascript" src="/assets/js/select2.min.js.gz"></script>
    <script type="text/javascript" src="/assets/js/angular-ui.min.js"></script>
    <script type="text/javascript" src="/assets/js/module.js"></script>
    <script type="text/javascript" src="/assets/js/select2.js"></script>
    <script type="text/javascript" src="/assets/js/modules/directives/jq/jq.js"></script>
    <!--<script type="text/javascript" src="/assets/js/coffee-script.js"></script>-->

    <script type="text/javascript">
        var CONFIG = {
            <?php if($user) echo 'user: '.json_encode($user).','; ?>
            wait: {
                message: '<span style="font-weight: 16px;"><img width="100px" height="100px" src="/assets/img/gipnozhaba.gif" /> Пожалуйста подождите...</span>'
            }
        }

        var blockUI = function() {
            //$.blockUI({ message: CONFIG.wait.message });
        }
        var unblockUI = function() {
            //$.unblockUI();
        }

        $.extend($.fn.typeahead.Constructor.prototype, {
            render: function(items) {
                var type = this.$element.attr('ui-type');

                var that = this,
                    text = this.query.replace(/@.*$/, ''),
                    val;

                if(type == 'email' || type == 'jabber' ) {

                    items = $(items).map(function(i, item) {
                        val = text + '@' + item;
                        i = $(that.options.item).attr('data-value', val);
                        i.find('a').html(that.highlighter(val));
                        return i[0];
                    });

                } else {
                    items = $(items).map(function(i, item) {
                        val =  item;
                        i = $(that.options.item).attr('data-value', val);
                        i.find('a').html(that.highlighter(val));
                        return i[0];
                    });
                }

                items.first().addClass('active');
                this.$menu.html(items);
                return this;
            }
        });

        // Array Remove - By John Resig (MIT Licensed)
        Array.prototype.remove = function(from, to) {
            var rest = this.slice((to || from) + 1 || this.length);
            this.length = from < 0 ? this.length + from : from;
            return this.push.apply(this, rest);
        };

    </script>
    <script type="text/javascript" src="/assets/js/main.js"></script>
</head>
<body ng-app="app">
<div id="theme_switcher" class="well" ng-controller="themeSwitcherCtrl"><button type="button" class="close" data-dismiss="alert">×</button>
    <div>Выбирите тему: </div>
    <span ng-repeat="theme in themes">
        <div><button style="margin-top: 10px;margin-left: 0px;" class="span1 btn btn-{{theme.class_}}" ng-click="setTheme(theme.name)">{{theme.name}}</button></div>
    </span>
</div>
<div class="alert" id="alert" style="display: none; position: fixed;top: 0px;left:0px; z-index:100000">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong></strong><span></span>
</div>
    <div class="container">
<!--window-->
        <!--auth window-->
        <div class="modal hide fade" id="auth" style="width: 250px;">
            <form  ng-controller="AuthCtrl" class="form-horizontal" ng-submit="submit()" style="margin: 0px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Авторизация</h3>
            </div>
            <div class="modal-body">

                    <div class="control-group">
                        Email<br/>
                        <input type="email" name="email" placeholder="Email" auto-complete ui-type="email"  ng-model="email" required>
                    </div>
                    <div class="control-group">
                        Пароль<br/>
                        <input type="password" name="password" placeholder="Пароль" required>
                    </div>
                    <input checked="checked" name="checked" type="checkbox" style="margin-top: -2px;"> Запоминть на месяц

            </div>
            <div class="modal-footer">
                <a href="#" class="btn" onclick="$('#auth').modal('hide');return false;">Отмена</a>
                <button href="#" class="btn btn-primary" ng-enable="enable">Продолжить</button>
            </div>
            </form>
        </div>

<!--/window-->
<!--static-->
        <!-- nav bar -->
	    <div class="navbar navbar-inverse" style="margin-bottom: 0px;">
            <div class="navbar-inner" style="border-radius: 4px 4px 0px 0px">
                    <a class="brand" href="#">&nbsp; <b><span style="color:#D00000">H</span>iAsm</b></a>
                    <div ng-controller="NavCtrl" style="width: 100%;">
                        <ul class="nav" style="margin: 0px;">
                            <li ng-repeat="(key,item) in items" ng-class="navClass(item.href, item.menu, key)">
                                <a href="{{item.href}}">{{item.name}}</a>
                            </li>
                            <li  class="search-query">
                                <a href="">Поиск</a>
                            </li>
                            <li class="search-divider divider-vertical"></li>
                        </ul>
                        <span style="display: inline-block;" class="pull-right">
                            <form class="navbar-search " action="" style="display: inline-block;">
                                <select style="width: 120px;margin-bottom: 0px;.box-sizing(border-box);display: none;">
                                    <option value="1">default</option>
                                </select>
                                <input type="text" class="span3 input-large search-query" style=".box-sizing(border-box);" placeholder="Найти..." x-webkit-speech speech>
                            </form>
                            <ul class="nav">
                                <li class="divider-vertical"></li>
                                <li class="dropdown user" ng-controller="UserCtrl" style="">
                                    <img class="avatar" ng-src="{{user.avatar || '/assets/img/man-icon.png'}}" id="user_image" width="36" height="36"/>
                                    <a style="display: inline-block;" href="#" class="dropdown-toggle" data-toggle="dropdown"><span id="user_name">{{user.login || 'Гость'}}</span> <b class="caret"></b></a>
                                    <ul class="dropdown-menu" ng-show="user.login">
                                        <li><a href="#!/forum/profile/{{user.login}}">Профиль</a></li>
                                        <li><a href ng-click="exit()">Выйти</a></li>
                                    </ul>
                                    <ul class="dropdown-menu" ng-hide="user.login">
                                        <li><a href="#" onclick="$('#auth').modal()">Войти</a></li>
                                        <li><a href="#!/forum/registration">Зарегистрироваться</a></li>
                                    </ul>
                                </li>
                            </ul>

                        </span>
                    </div>
            </div>
	    </div>
        <!--mini nav bar-->
        <div class="navbar" style="margin-bottom: 0px;">
            <div class="navbar-inner sub_nav" ng-controller="NavCtrl">
                <ul class="nav" style="min-height: 30px;height: 30px;">

                    <li class="sub_nav" ng-repeat="(i,item) in getSubMenu()" id="hover_{{i}}">
                        <a href="{{item.href}}" onclick="{{item.onclick}}" style="padding: 4px;">{{item.name}}</a>
                        <div style="background-color: #{{subColors[i]}};padding-bottom: 4px;margin-top: -2px;text-shadow: 0 1px 0 #000;"></div>
                    </li>
                    <li class="sub_nav" id="hover_4" ng-show="isThread()">
                        <a href="" onclick="" style="padding: 4px;text-shadow: 0 0px 0 #000;">Написать</a>
                        <div style="background-color: #{{subColors[4]}};padding-bottom: 4px;margin-top: -2px;text-shadow: 0 1px 0 #000;"></div>
                    </li>
                    <li class="sub_nav"></li>
                </ul>
                <span class="pull-right visible-desktop" style="padding-top: 4px;">
                    Самый высокий рейтинг у <a>сообщения <span class="badge badge-success">+5</span>
                    </a> оставленное сегодня - <span class="label label-info"><i class="icon icon-user"></i> Леонид</span>
                </span>
            </div>
        </div>
<!--/static-->
        <div ng-view></div>
<!--templates-->
            <!--DevCtrl-->
            <script type="text/ng-template" id="dev.html">
                <table class="table table-bordered" ng-controller="SectionsCtrl" style="border-radius: 0;margin-bottom: 0;">
                    <thead>
                    </thead>
                    <tbody ng-repeat="d in log">
                        <tr>
                            <td style="width: 120px" colspan="{{cols(d.label)}}">
                                <img ng-src="/assets/img/{{d.type}}.png" style="width: 32px;height: 32px;"/>
                                <span ng-show="d.label" ng-class="class(d.type)" > {{d.label}}</span>
                                <span ng-hide="d.label"><b>{{d.message}}</b></span>
                            </td>
                            <td ng-show="d.label" >
                                {{d.message}}
                            </td>
                        </tr>
                    </tbody>
                </table>

            </script>
            <!--UserCtrl-->
            <script type="text/ng-template" id="reg.html">
                <div style="border: 1px solid #dedede;padding: 10px;">
                    <form ng-controller="AddUserFormCtrl" name="form" class="form-horizontal" ng-submit="submit()" novalidate>
                        <fieldset>
                            <div class="control-group" ng-class="{warning: form.login.$dirty && form.login.$invalid, success: form.login.$valid}">
                                <label>Логин:</label>
                                <input type="text" autofocus  name="login" ng-model="fields.login" placeholder="" class="input-large" required/>
                                <div class="help-inline" ng-show="form.login.$dirty && form.login.$invalid">Ошибка:
                                    <span class="help-inline" ng-show="form.login.$error.required">Обязательное поле.</span>
                                </div>
                            </div>
                            <div class="control-group" ng-class="{warning: form.email.$dirty && form.email.$invalid, success: form.email.$valid}">
                                <label>Email:</label>
                                <input type="email" name="email" ng-model="fields.email" placeholder="" class="input-large"  auto-complete  ui-type="email" required/>
                                <div class="help-inline" ng-show="form.email.$dirty && form.email.$invalid">Ошибка:
                                    <span ng-show="form.email.$error.required">Обязательное поле.</span>
                                    <span ng-show="form.email.$error.email">Не верный формат email.</span>
                                </div>
                            </div>
                            <div class="control-group" ng-class="{warning: form.password.$dirty && form.password.$invalid, success: form.password.$valid}">
                                <label>Пароль:</label>
                                <input type="text" name="password" ng-model="fields.password" placeholder="" class="input-large" required/>
                                <div class="help-inline" ng-show="form.password.$dirty && form.password.$invalid">Ошибка:
                                    <span class="help-inline" ng-show="form.password.$error.required">Обязательное поле.</span>
                                </div>
                            </div>
                            <br />
                            <button class="btn btn-primary" ng-disabled="form.$invalid">Продолжить</button>
                        </fieldset>
                    </form>
                </div>
            </script>
            <!--SectionsCtrl-->
            <script type="text/ng-template" id="sections.html" ng-controller="SectionsCtrl">
                <table class="table table-bordered" style="border-radius: 0;margin-bottom: 0;">
                    <thead>
                    <tr>
                        <td><b><i class="icon icon-list"></i> Форум</b></td>
                        <td style="width: 60px;text-align: center"><b><i class="icon icon-tasks"></i> Темы</b></td>
                        <td class="message_header"><b><i class="icon icon-envelope"></i> <span class="message_label">Сообщения</span></b></td>
                        <td style="width: 100px;"><b><i class="icon icon-time"></i> Последнее</b></td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="section in sections" data-parent="{{section.parent}}" data-id="{{section.id}}" ng-hide="getState(section.parent) && section.parent != 0">
                        <!--template section name-->
                        <td colspan="4" class="header" ng-show="section.parent == 0">
                            <img ng-src="/assets/img/{{section.icon || 'circle_grey_24_ns.png'}}" width="24" height="24"/>
                            <b> {{section.name}}</b>
                            <a class="btn btn-small" ng-click="minimize($event)" style="float: right" href="" data-id="{{section.id}}">
                                <span ng-hide="getState(section.id)"><i class="icon-minus"></i> Свернуть</span>
                                <span ng-show="getState(section.id)"><i class="icon-plus"></i> Развернуть</span>
                            </a>
                        </td>
                        <!--template section-->
                        <td class="body" ng-hide="section.parent == 0">

                            <div style="float:left;padding-right: 5px;">
                                <img ng-src="/assets/img/{{section.icon || 'circle_grey_24_ns.png'}}" width="24" height="24"/>
                            </div>
                            <a href="#!/forum/section/{{section.id}}/page/1"><b> {{section.name}}</b></a>
                            <div class="description">{{section.description}}</div>

                        </td>
                        <td ng-hide="section.parent == 0" style="text-align:center">
                            {{section.count}}
                        </td>
                        <td ng-hide="section.parent == 0" style="text-align:center">
                            {{section.answers}}
                        </td>
                        <td ng-hide="section.parent == 0" style="text-align: center;padding: 4px;">
                            <span class="date_small">{{section.last*1000 | date:'d.MM.yy в HH:mm'}}</span>
                            <span class="date">{{section.last*1000 | date:'d MMM yyyy в HH:mm'}}</span>
                        </td>
                        <!--/template section-->
                    </tr>
                    </tbody>
                </table>
                <div class="border" style="border-top: 0px" ng-hide="sections.length">
                    Загрузка списка форумов.
                </div>
            </script>
            <!--services.htm-->
            <script type="text/ng-template" id="services.html">
                <div style="border: 1px solid #dedede;padding: 10px;">
                services
                </div>
            </script>
            <!--profile.htm-->
            <script type="text/ng-template" id="profile.html">
                <div style="border: 1px solid #dedede;padding: 10px;">
                    <h4>Профиль пользователя {{profile.login}}</h4>
                    <hr style="margin-bottom: 10px;margin-top: 10px">
                    <img class="avatar" ng-src="{{profile.avatar}}" alt=""/>
                    <span>
                        <div><b>Статус <img src="/assets/img/user_male.png" alt=""></b>  {{profile.status}}</div>
                        <div><b>Зарегистрирован <img src="/assets/img/clock_blue.png" alt=""></b>  {{profile.created_at | date:'d MMM yyyy в HH:mm'}}</div>
                        <div><b>Последний вход <img src="/assets/img/stock_timer.png" alt=""></b>  {{profile.updated_at | date:'d MMM yyyy в HH:mm'}}</div>
                        <div><b>О себе <img src="/assets/img/information.png" alt=""></b>  {{profile.info}}</div>
                        <div><b>Подпись <img src="/assets/img/sign.png" alt=""></b>  {{profile.sign}}</div>
                        <div><b>Дата рождения <img src="/assets/img/cake.png" alt=""></b>  {{profile.birthday }}</div>
                        <div><b>Ответов <img src="/assets/img/comment_dots.png" alt=""></b>  {{profile.answers }}</div>
                        <div><b>Золото <img src="/assets/img/coin_gold.png" alt=""></b>  {{profile.gold || 0}}</div>
                        <div><b>Рейтинг <img src="/assets/img/star__on.png" width="16" height="16" alt=""></b>  {{profile.rating}}  </div>
                        <div><b>Тема оформления <img src="/assets/img/themes.png" alt=""></b>  {{profile.style || 'Стандартная'}}  </div>
                        <div><b>Сайт <img src="/assets/img/globe.png" alt=""></b>  {{profile.site}}  </div>
                        <div><b>icq <img src="/assets/img/icq_online.png" alt=""></b>  {{profile.icq}}  </div>
                        <div><b>jabber <img src="/assets/img/jabber.png" alt=""></b>  {{profile.jabber}}  </div>
                        <div><b>Страна <img src="/assets/img/earth-icon.png" alt=""></b>  {{profile.country}}  </div>
                        <div><b>Город <img src="/assets/img/city.png" alt=""></b>  {{profile.city}}  </div>
                        <div><b>Последнее сообщение <img src="/assets/img/email.png" alt=""></b>  {{profile.last_msg }}  </div>
                        <div ngrepeat="(k,v) in profile">{{k}}  {{v}}</div>
                        <a class="btn" href="#!/forum/profile/{{profile.login}}/edit">edit profile page</a>
                    </span>
                </div>
            </script>
<!--profile_edit.htm-->
<script type="text/ng-template" id="profile_edit.html">
    <div style="border: 1px solid #dedede;padding: 10px;">
        <h4>Профиль пользователя {{profile.login}}</h4>
        <hr style="margin-bottom: 10px;margin-top: 10px">
        <form style="padding-left: 20px" ng-controller="ProfileFormCtrl" name="form" class="form-horizontal" ng-submit="submit()" novalidate>
            <filedset>
                <span>
                    <div><b>Аватар:</b></div>
                    <img class="avatar" ng-src="{{profile.avatar}}" alt=""/>
                </span>

                <div>
                    <div class="control-group"><b>О себе:</b> <textarea ng-model="profile.info" name=""
                                                                        style="height: 50px;display: block;width: 220px;"
                                                                        cols="3"
                                                                        rows="2"></textarea></div>
                    <div class="control-group"><b>Подпись:</b> <textarea ng-model="profile.sign" name=""
                                                                         style="height: 30px;display: block;width: 220px;"
                                                                         cols="3"
                                                                         rows="2"></textarea>
                    </div>
                    <div class="control-group"><b>Дата рождения:</b>

                        <div style="display: block;" class="input-append date datapicker"
                             data-date="{{profile.birthday}}" data-date-format="dd.mm.yyyy">
                            <input style="width: 180px" class="datapicker" size="16"
                                   type="text" ng-model="profile.birthday">
                            <span class="add-on"><i class="icon-th"></i></span>
                        </div>
                    </div>
                    <div class="control-group"><b>Тема оформления:</b>

                        <div>
                            <select name="" id="">
                                <option value="">{{profile.style || 'Стандартная'}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group"><b>Сайт:</b>

                        <div><input type="text" ng-model="profile.site"/></div>
                    </div>
                    <div class="control-group"><b>icq:</b>

                        <div><input type="text" ng-model="profile.icq"/></div>
                    </div>
                    <div class="control-group"><b>jabber:</b>

                        <div><input auto-complete ui-type="jabber" type="text"
                                    ng-model="profile.jabber"/></div>
                    </div>
                    <div class="control-group"><b>Страна:</b>

                        <div><input auto-complete ui-type="country" type="text"
                                    ng-model="profile.country"/></div>
                    </div>
                    <div class="control-group"><b>Город:</b>

                        <div><input auto-complete ui-type="city" type="text"
                                    ng-model="profile.city"/></div>
                    </div>
                    <button type="submit" class="btn btn-success">Сохранить</button>
                </div>
            </filedset>
        </form>
    </div>
</script>
<!--newThread.html-->
<script src = "/assets/js/wysihtml5-0.3.0.js.gz" ></script>
<link rel="stylesheet" href="/assets/css/stylesheet.css">
<script src="/assets/js/parser_rules/advanced.js"></script>
<style type="text/css">
    span.quote {
        color: red;
        background-color: green;
    }

    #toolbar [data-wysihtml5-action] {
        float: right;
        margin-right: 10px;
    }

    #toolbar,
    textarea {
        width: 100%;
        padding: 5px;
    }

    textarea {
        height: 280px;
        box-sizing: boder-box;
        -webkit-box-sizing: border-box;
        -ms-box-sizing: border-box;
        -moz-box-sizing: border-box;
        font-family: Verdana;
        font-size: 11px;

        border: 1px solid #cccccc;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        -webkit-transition: border linear 0.2s, box-shadow linear 0.2s;
        -moz-transition: border linear 0.2s, box-shadow linear 0.2s;
        -o-transition: border linear 0.2s, box-shadow linear 0.2s;
        transition: border linear 0.2s, box-shadow linear 0.2s;

        resize:vertical;
    }

    ul.commands>li {
        width: 40px;
        height: 40px;
    }
    ul.commands{
        margin: 0;
        height: 31px;
    }
</style>

<script type="text/ng-template" id="newThread.html">
    <div style="border: 1px solid #dedede;padding: 10px;">
        <form ng-controller="AddThreadFormCtrl" name="form" class="form-horizontal" ng-submit="submit()" novalidate>
            <fieldset style="width: 100%;" >
                <div class="control-group" ng-class="required(form, 'title')">
                    <label>Заголовок:</label>
                    <input type="text" autofocus name="title" ng-model="fields.title" placeholder="Название темы" class="input-xxlarge" required/>
                </div>
                <div class="control-group" ng-class="required(form, 'section')">
                    <label>Раздел:</label><?php /*
                    <select name="section" ng-model="fields.section" ng-options="s.name group by s.parent_name for (i,s) in sections | filter:child" required>
                        <option value="">Выберите раздел</option>
                    </select>*/ ?>
                    <select name="section" ng-model="fields.section" required>
                        <option value="" >Выберите раздел</option>
                        <optgroup label="{{section.name}}" ng-repeat="section in sections | filter:root">
                            <option ng-repeat="s in sections | filter:{parent:section.id}" ng-selected="s.id == section_id" value="{{s.id}}">{{s.name}}</option>
                        </optgroup>

                    </select>
                </div>
                <div class="control-group">
                    <label>Сообщение:</label>
                    <div id="wysihtml5-editor-toolbar">
                        <header style="width: auto;position: static;">
                            <ul class="commands">
                                <li data-wysihtml5-command="bold" title="Жирный (CTRL + B)" class="command"></li>
                                <li data-wysihtml5-command="italic" title="Курсив (CTRL + I)" class="command"></li>
                                <li data-wysihtml5-command="underline" title="Подчёркнутый (CTRL + U)" class="command"></li>
                                <li data-wysihtml5-command="insertUnorderedList" title="Неупорядоченый список" class="command"></li>
                                <li data-wysihtml5-command="insertOrderedList" title="Нумерованый список" class="command"></li>
                                <li data-wysihtml5-command="createLink" title="Вставить ссылку" class="command"></li>
                                <li data-wysihtml5-command="insertImage" title="Вставить изображение" class="command"></li>
                                <li data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="h1" title="Заголовок" class="command"></li>
                                <li data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="h2" title="Подзаголовок" class="command"></li>
                                <li data-wysihtml5-command-group="foreColor" class="fore-color" title="Цвет текста" class="command">
                                    <ul>
                                        <li data-wysihtml5-command="foreColor" data-wysihtml5-command-value="silver"></li>
                                        <li data-wysihtml5-command="foreColor" data-wysihtml5-command-value="gray"></li>
                                        <li data-wysihtml5-command="foreColor" data-wysihtml5-command-value="maroon"></li>
                                        <li data-wysihtml5-command="foreColor" data-wysihtml5-command-value="red"></li>
                                        <li data-wysihtml5-command="foreColor" data-wysihtml5-command-value="purple"></li>
                                        <li data-wysihtml5-command="foreColor" data-wysihtml5-command-value="green"></li>
                                        <li data-wysihtml5-command="foreColor" data-wysihtml5-command-value="olive"></li>
                                        <li data-wysihtml5-command="foreColor" data-wysihtml5-command-value="navy"></li>
                                        <li data-wysihtml5-command="foreColor" data-wysihtml5-command-value="blue"></li>
                                    </ul>
                                </li>
                                <li data-wysihtml5-command="insertSpeech" title="Голосовой ввод" class="command"></li>
                                <?php /*<li data-wysihtml5-action="change_view" title="Показать HTML" class="action"></li>
                                */ ?><li data-wysihtml5-command="table" data-wysihtml5-command-value=""></li>
                            </ul>
                        </header>
                        <div data-wysihtml5-dialog="createLink" style="display: none;">
                            <label>
                                Url:
                                <input data-wysihtml5-dialog-field="href" value="http://">
                            </label>
                            <a data-wysihtml5-dialog-action="save">OK</a>&nbsp;<a data-wysihtml5-dialog-action="cancel">Отмена</a>
                        </div>

                        <div data-wysihtml5-dialog="insertImage" style="display: none;">
                            <label>
                                Url изображения:
                                <input data-wysihtml5-dialog-field="src" value="http://">
                            </label>
                            <a data-wysihtml5-dialog-action="save">OK</a>&nbsp;<a data-wysihtml5-dialog-action="cancel">Отмена</a>
                        </div>

                    </div>
                        <textarea name="message" ng-model="fields.message" id="wysihtml5-editor" placeholder="Введите сообщение ...">

                        </textarea>

                </div>
                <div class="control-group pull-right" style="padding-right: 20px">
                    <SPAN>Cледить за ответами по e-mail: </SPAN>
                    <input name="email" style="margin-top: -2px" auto-complete ui-type="email"  ng-model="fields.email" type="checkbox">
                </div>
                <br />
                       <button class="btn btn-primary" ng-disabled="form.$invalid" style="margin-right: 20px">Продолжить</button>


            </fieldset>
        </form>
    </div>
</script>
<!--sha-->
<script type="text/ng-template" id="sha.html">

</script>
<!--ThreadsCtrl-->
  <script type="text/ng-template" id="threads.html">
                    <div id="bread" style="border-bottom:0">
                        <ui-breadcrumb></ui-breadcrumb>
                        <?php /*<ul class="breadcrumb" style="margin: 0px">
                            <li><a href="#!/forum/">Форум</a> <span class="divider">/</span></li>
                            <li class="active">{{section.name || 'Последние'}}</li>
                        </ul>*/ ?>
                    </div>
                    <table class="table table-bordered" style="border-radius: 0;margin-bottom: 0;">
                        <thead>
                        <tr>
                            <td colspan="6">
                                <ui-pagination cur="pagination.cur" total="pagination.total" display="5"></ui-pagination>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left: 10px"><b><i class="icon icon-list"></i> Темы</b></td>
                            <td class="rating_text"><b><i class="icon icon-star"></i> Оценка</b></td>
                            <td class="answer_text"><b><i class="icon icon-envelope"></i> Ответов</b></td>
                            <td style="width: 70px;"><b><i class="icon icon-user"></i> Автор</b></td>
                            <td class="views_text"><b><i class="icon icon-film"></i> <span class="views_text">Просмотров</span></b></td>
                            <td style="width: 100px;"><b><i class="icon icon-time"></i> Последнее</b></td>
                        </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="t in threads">
                                <td style="padding-left: 10px">
                                    <img ng-src="/assets/img/circle_grey_24_ns.png" width="24" height="24"/> <a href="#!/forum/section/{{t.section}}/thread/{{t.id}}/page/1" ng-click="threadClick(t)">{{t.name}}</a>
                                    <i ng-show="t.closed" class="icon icon-lock pull-right"></i>
                                    <i ng-show="t.attached" class="icon icon-exclamation-sign pull-right"></i>
                                </td>
                                <td>
                                    <rating rating = "{{t.rating}}" thread = "{{t.id}}"></rating>
                                    <img class="star" ng-repeat="(i, v) in rating('start')" ng-src="/assets/img/{{ rating(t.rating, i) }}" width="22" height="22"/>
                                </td>
                                <td style="text-align: center">{{t.answers || 0}}</td>
                                <td style="text-align: center"><i class="icon icon-user"></i> {{t.username || 'Гость'}}</td>
                                <td style="text-align: center">{{t.views || 0}}</td>
                                <td style="text-align: center;padding: 2px;">{{t.last*1000 | date:'d MMM yyyy в HH:mm'}}</td>
                            </tr>
                            <tr ng-hide="threads"><td colspan="6">Нет результатов</td></tr>
                            <tr>
                                <td colspan="6">
                                    <ui-pagination cur="pagination.cur" total="pagination.total" display="5"></ui-pagination>
                                </td>
                            </tr>
                        </tbody>
                    </table>
            </script>
<!--AnswersCtrl-->
<script type="text/ng-template" id="answers.html" >
            <div style="border: 1px solid #dedede;border-bottom:0">
                <ui-breadcrumb></ui-breadcrumb>
                <span style="float: right;margin-top: -30px;margin-right:10px;">
                    <i ng-show="info.closed" class="icon icon-lock"></i>
                    <i ng-show="info.attached" class="icon icon-exclamation-sign"></i>
                </span>
            </div>
            <table class="table table-bordered" style="border-radius: 0;margin-bottom: 0;">
                <thead>
                <tr>
                    <td colspan="6">
                        <ui-pagination cur="pagination.cur" total="pagination.total" display="5"></ui-pagination>
                    </td>
                </tr>
                <tr>
                    <td style="width: 120px;"><b><i class="icon icon-user"></i> Автор</b></td>
                    <td><b><i class="icon icon-envelope"></i> Сообщение</b>
                        <a href="" class="pull-right" ng-hide="admin_mode" ng-click="admin_mode = true">Режим администратора</a>
                        <div class="btn-group pull-right" ng-show="admin_mode">
                            <button  ui-jq="tooltip" title="Обычный режим" ng-click="admin_mode = false" class="btn btn-mini"><i class="icon icon-off"></i> Обычный режим</button>
                            <button  ui-jq="tooltip" title="Переименовать" class="btn btn-success btn-mini" ng-click="edit_name_show = true"><i class="icon icon-pencil"></i> Переименовать</button>
                            <button  ui-jq="tooltip" title="Прикрепить/Открепить" class="btn btn-info btn-mini" ng-click="edit_attach(info.attached)"><i class="icon icon-exclamation-sign"></i> <span ng-hide="info.attached">Прикрепить</span><span ng-show="info.attached">Открепить</span></button>
                            <button  ui-jq="tooltip" title="Переместить" class="btn btn-primary btn-mini" ng-click="move_thread_show = true"><i class="icon icon-move"></i> Переместить</button>
                            <button  ui-jq="tooltip" title="Закрыть/Открыть" class="btn btn-warning btn-mini" ng-click="edit_closed(info.closed)"><i class="icon icon-lock"></i> <span ng-hide="info.closed">Закрыть</span><span ng-show="info.closed">Открыть</span></button>
                            <button  ui-jq="tooltip" title="Удалить" class="btn btn-danger btn-mini" ng-click="thread_delete()"><i class="icon icon-remove"></i> Удалить</button>
                        </div>
                    </td>
                </tr>
                <tr ng-show="edit_name_show">
                    <td colspan="6">
                        <span style="padding-top: 2px">Название: </span>
                        <input type="" value="{{info.name}}" ng-model="new_name"/>
                        <button class="btn btn-small btn-success" ng-click="name_save(new_name)"><i class="icon-white icon-ok"></i> Сохранить</button>
                        <button class="btn btn-small btn-error" ng-click="edit_name_show = false"><i class="icon icon-ok"></i> Отмена</button>
                    </td>
                </tr>
                <tr ng-show="move_thread_show">
                    <td colspan="6">
                        <span style="padding-top: 2px">Раздел: </span>
                        <select name="section" ng-model="fields.section" style="margin-bottom: 0px;">
                            <option value="" >Выберите раздел</option>
                            <optgroup label="{{section.name}}" ng-repeat="section in sections | filter:root">
                                <option ng-repeat="s in sections | filter:{parent:section.id}" ng-selected="s.id == section_id" value="{{s.id}}">{{s.name}}</option>
                            </optgroup>
                        </select>
                        <button class="btn btn-small btn-success" ng-click="move_thread(fields.section)"><i class="icon-white icon-ok"></i> Сохранить</button>
                        <button class="btn btn-small btn-error" ng-click="move_thread_show = false"><i class="icon icon-ok"></i> Отмена</button>
                    </td>
                </tr>
                </thead>
                <tbody ng-repeat="(i,a) in answers">
                        <tr>
                            <td></td>
                            <td style="font-size: 10px;padding: 2px;">
                                Добавлено {{a.created_at*1000 | date:'d MMM yyyy в HH:mm'}} 
                                <span class="btn-group" style="margin-left: 40px">
                                    <button ui-jq="tooltip" title="Цитировать" ng-click="quote(i)" class="btn btn-mini">
                                        <i class="icon icon-edit"></i> Цитировать
                                    </button>
                                    <button ui-jq="tooltip" title="Изменить" class="btn btn-primary btn-mini" ng-click="edit(a)">
                                        <i class="icon icon-pencil"></i> Изменить
                                    </button>
                                </span>
                                <div class="btn-group pull-right" ng-show="admin_mode">
                                    <button ui-jq="tooltip" title="Замечание" ng-click="notice_show=!notice_show" class="btn btn-info btn-mini"><i class="icon icon-warning-sign"></i> Замечание</button>
                                    <button ui-jq="tooltip" title="Отрезать" disabled class="btn btn-success btn-mini"><i class="icon icon-book"></i> Отрезать</button>
                                    <button ui-jq="tooltip" title="Бан" disabled class="btn btn-warning btn-mini"><i class="icon icon-ban-circle"></i> Бан</button>
                                    <button ui-jq="tooltip" title="Удалить" ng-click="answers = delete(answers, i)" class="btn btn-danger btn-mini"><i class="icon icon-remove"></i> Удалить</button>
                                </div>
                            </td>
                        </tr>
                        <tr ng-show="notice_show">
                            <td colspan="6">
                                <span style="padding-top: 2px">Коментарий: </span>
                                <input style="display: block;width: 98%" value="{{a.notice}}" type="text" ng-model="notice"/>
                                <div>Тип:</div>
                                <select ui-select2 name="notice_type" ng-model="notice_type" style="margin-right: 20px;width: 200px">
                                    <option value="remark">Замечание</option>
                                    <option value="notice">Уведомление</option>
                                    <option value="price">Похвала</option>
                                </select>
                                <span class="pull-right">
                                    <button class="btn btn-small btn-success" ng-click="a = notice_save(notice, notice_type, a); notice_show = false; notice = ''"><i class="icon-white icon-ok"></i> Сохранить</button>
                                    <button class="btn btn-small btn-error" ng-click="notice_show = false"><i class="icon icon-ok"></i> Отмена</button>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center" style="text-align: center;">
                                <b ng-hide="a.username" style="color: green;"> {{'Гость'}}</b>
                                <a ng-show="a.username" href="#!/forum/profile/{{a.username}}"><b style="color: green;">{{a.username}}</b></a>
                                <div>Участник</div>
                                <img style="max-height: 100px;max-width:100px;" class="avatar" ng-src="{{a.avatar || '/assets/img/noavatar.png'}}" alt="avatar"/>
                                <div>Ответов: <span class="badge" style="border-radius: 0px">222</span></div>
                                <div>Рейтинг:</div>
                                <div class="btn-group" ng-controller="UserCtrl">
                                    <button class="btn btn-mini btn-danger" ng-disabled="!a.rating" ng-click="minus(a,answers)">-</button>
                                    <span class="btn btn-mini" ng-disabled="!a.rating"  style="overflow: hidden;">
                                        <span ng-hide="a.rating">0</span>
                                        <span class="rating_num {{a.id}}_{{j}}" ng-repeat="(j,r) in a.rating">{{r}}</span>
                                    </span>
                                    <button class="btn btn-mini btn-success" ng-disabled="!a.rating"  ng-click="plus(a,answers)">+</button>
                                </div>
                            </td>
                            <td ng-bind-html-unsafe="a.message"></td>
                        </tr>
                        <tr ng-show="a.notice" class="notice_type_{{notice_types[ a.notice_type ]}}">
                            <td style="text-align: center">
                                <img ng-src="/assets/img/{{notice_types[ a.notice_type ]}}.png" alt=""/> username
                            </td>
                            <td>
                                {{a.notice}}
                                <button  ui-jq="tooltip" title="Удалить" class="btn btn-mini btn-danger pull-right" ng-show="admin_mode" ng-click="a = notice_del(a)"><i class="icon icon-remove"></i> Удалить</button>
                            </td>
                        </tr>
                </tbody><!-- angularjs ((((-->
                <tr ng-hide="answers"><td colspan="6">Нет результатов</td></tr>
                 <tr>
                     <td colspan="6" style="border-radius: 0px;">
                         <ui-pagination cur="pagination.cur" total="pagination.total" display="5"></ui-pagination>
                     </td>
                 </tr>
            </table>
            <div style="border: 1px solid #dedede;border-top: 0px;padding: 10px;">
                <form style="display: none" ng-controller="AddAnswerFormCtrl" class="form-horizontal" ng-submit="submit()" ng-hide="closed(info.closed)">
                    <fieldset style="width: 100%;" >
                        <div class="control-group">
                            <label>Сообщение:</label>
                            <div id="wysihtml5-editor-toolbar">
                                <header style="width: auto;position: static;">
                                    <ul class="commands">
                                        <li data-wysihtml5-command="bold" title="Жирный (CTRL + B)" class="command"></li>
                                        <li data-wysihtml5-command="italic" title="Курсив (CTRL + I)" class="command"></li>
                                        <li data-wysihtml5-command="underline" title="Подчёркнутый (CTRL + U)" class="command"></li>
                                        <li data-wysihtml5-command="insertUnorderedList" title="Неупорядоченый список" class="command"></li>
                                        <li data-wysihtml5-command="insertOrderedList" title="Нумерованый список" class="command"></li>
                                        <li data-wysihtml5-command="createLink" title="Вставить ссылку" class="command"></li>
                                        <li data-wysihtml5-command="insertImage" title="Вставить изображение" class="command"></li>
                                        <li data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="h1" title="Заголовок" class="command"></li>
                                        <li data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="h2" title="Подзаголовок" class="command"></li>
                                        <li data-wysihtml5-command-group="foreColor" class="fore-color" title="Цвет текста" class="command">
                                            <ul>
                                                <li data-wysihtml5-command="foreColor" data-wysihtml5-command-value="silver"></li>
                                                <li data-wysihtml5-command="foreColor" data-wysihtml5-command-value="gray"></li>
                                                <li data-wysihtml5-command="foreColor" data-wysihtml5-command-value="maroon"></li>
                                                <li data-wysihtml5-command="foreColor" data-wysihtml5-command-value="red"></li>
                                                <li data-wysihtml5-command="foreColor" data-wysihtml5-command-value="purple"></li>
                                                <li data-wysihtml5-command="foreColor" data-wysihtml5-command-value="green"></li>
                                                <li data-wysihtml5-command="foreColor" data-wysihtml5-command-value="olive"></li>
                                                <li data-wysihtml5-command="foreColor" data-wysihtml5-command-value="navy"></li>
                                                <li data-wysihtml5-command="foreColor" data-wysihtml5-command-value="blue"></li>
                                            </ul>
                                        </li>
                                        <li data-wysihtml5-command="insertSpeech" title="Голосовой ввод" class="command"></li>
                                        <?php /*<li data-wysihtml5-action="change_view" title="Показать HTML" class="action"></li>
                                */ ?><li data-wysihtml5-command="table" data-wysihtml5-command-value=""></li>
                                    </ul>
                                </header>
                                <div data-wysihtml5-dialog="createLink" style="display: none;">
                                    <label>
                                        Url:
                                        <input data-wysihtml5-dialog-field="href" value="http://">
                                    </label>
                                    <a data-wysihtml5-dialog-action="save">OK</a>&nbsp;<a data-wysihtml5-dialog-action="cancel">Отмена</a>
                                </div>

                                <div data-wysihtml5-dialog="insertImage" style="display: none;">
                                    <label>
                                        Url изображения:
                                        <input data-wysihtml5-dialog-field="src" value="http://">
                                    </label>
                                    <a data-wysihtml5-dialog-action="save">OK</a>&nbsp;<a data-wysihtml5-dialog-action="cancel">Отмена</a>
                                </div>

                            </div>
                            <textarea name="message" id="wysihtml5-editor" required>
                            </textarea>

                        </div>
                        <input type="hidden" name="thread" value="{{thread_id}}"/>
                        <span class="btn btn-primary" ng-enable="enable" style="margin-right: 20px">Написать</span>

                    </fieldset>
                </form>
                <span ng-show="info.closed">
                    <div class="alert alert-error">
                        Тема закрыта для обсуждения!
                    </div>
                </span>
            </div>
</script>
<!--/templates-->
<!--foot-->
        <div class="navbar navbar-inverse" style="margin-bottom: 0px;">
                <div class="navbar-inner" style="border-radius: 0px 0px 4px 4px;text-align: center;">
                    footer
                </div>
            </div>
    </div>
<?php /* ?>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">(function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter19483978 = new Ya.Metrika({id:19483978, webvisor:true, clickmap:true, trackLinks:true, accurateTrackBounce:true, trackHash:true, ut:"noindex"}); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="//mc.yandex.ru/watch/19483978?ut=noindex" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<?php //*/ ?>
</body>
</html>