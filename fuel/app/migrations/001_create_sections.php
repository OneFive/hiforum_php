<?php

namespace Fuel\Migrations;

class Create_sections
{
	public function up()
	{
		\DBUtil::create_table('sections', array(
			'id' => array('constraint' => 5, 'type' => 'smallint', 'auto_increment' => true),
			'name' => array('constraint' => 50, 'type' => 'varchar'),
			'description' => array('constraint' => 50, 'type' => 'varchar'),
			'icon' => array('constraint' => 50, 'type' => 'varchar'),
			'parent' => array('constraint' => 5, 'type' => 'smallint'),
			'sort' => array('constraint' => 5, 'type' => 'smallint'),
			'created_at' => array('constraint' => 11, 'type' => 'int'),
			'updated_at' => array('constraint' => 11, 'type' => 'int'),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('sections');
	}
}