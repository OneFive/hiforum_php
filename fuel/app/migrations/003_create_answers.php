<?php

namespace Fuel\Migrations;

class Create_answers
{
	public function up()
	{
		\DBUtil::create_table('answers', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'author' => array('constraint' => 11, 'type' => 'int'),
			'thread' => array('constraint' => 11, 'type' => 'int'),
			'notice' => array('constraint' => 150, 'type' => 'varchar'),
			'notice_type' => array('constraint' => 1, 'type' => 'smallint'),
			'notice_author' => array('type' => 'smallint'),
			'message' => array('type' => 'text'),
			'created_at' => array('constraint' => 11, 'type' => 'int'),
			'updated_at' => array('constraint' => 11, 'type' => 'int'),
			'created_at' => array('constraint' => 11, 'type' => 'int'),
			'updated_at' => array('constraint' => 11, 'type' => 'int'),
            'deleted' => array('constraint' => 3, 'type' => 'int'),
		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('answers');
	}
}