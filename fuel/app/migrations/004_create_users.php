<?php

namespace Fuel\Migrations;

class Create_users
{
	public function up()
	{
		\DBUtil::create_table('users', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'login' => array('constraint' => 50, 'type' => 'varchar'),
			'name' => array('constraint' => 50, 'type' => 'varchar'),
			'family' => array('constraint' => 100, 'type' => 'varchar'),
			'answers' => array('constraint' => 11, 'type' => 'int'),
			'rating' => array('constraint' => 11, 'type' => 'int'),
			'plus' => array('constraint' => 11, 'type' => 'int'),
			'minus' => array('constraint' => 11, 'type' => 'int'),
			'info' => array('constraint' => 100, 'type' => 'varchar'),
			'sign' => array('constraint' => 150, 'type' => 'varchar'),
			'birthday' => array('constraint' => 11, 'type' => 'varchar'),
			'email' => array('constraint' => 150, 'type' => 'varchar'),
			'site' => array('constraint' => 150, 'type' => 'varchar'),
			'icq' => array('constraint' => 11, 'type' => 'int'),
			'themes' => array('type' => 'smallint'),
			'last_msg' => array('constraint' => 11, 'type' => 'int'),
			'u_schemes' => array('type' => 'smallint'),
			'u_companents' => array('type' => 'smallint'),
			'p_schemes' => array('type' => 'smallint'),
			'p_companents' => array('type' => 'smallint'),
			'reg_blog' => array('type' => 'bit'),
			'ip_auth' => array('type' => 'bit'),
			'status' => array('constraint' => 4, 'type' => 'INT'),
			'ban' => array('constraint' => 11, 'type' => 'int'),
			'bans' => array('type' => 'smallint'),
			'avatar' => array('constraint' => 150, 'type' => 'varchar'),
			'jabber' => array('constraint' => 150, 'type' => 'varchar'),
			'p_email' => array('type' => 'bit'),
			'gold' => array('type' => 'smallint'),
			'password' => array('constraint' => 50, 'type' => 'varchar'),
			'token' => array('constraint' => 50, 'type' => 'varchar'),
            'token_time' => array('constraint' => 11, 'type' => 'int'),
			'location' => array('constraint' => 50, 'type' => 'varchar'),
			'city' => array('constraint' => 50, 'type' => 'varchar'),
			'country' => array('constraint' => 50, 'type' => 'varchar'),
			'language' => array('constraint' => 50, 'type' => 'varchar'),
			'style' => array('constraint' => 50, 'type' => 'varchar'),
			'created_at' => array('constraint' => 11, 'type' => 'int'),
			'updated_at' => array('constraint' => 11, 'type' => 'int'),
			'created_at' => array('constraint' => 11, 'type' => 'int'),
			'updated_at' => array('constraint' => 11, 'type' => 'int'),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('users');
	}
}