<?php

namespace Fuel\Migrations;

class Create_threads
{
    public function up()
    {
        \DBUtil::create_table('threads', array(
            'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
            'name' => array('constraint' => 150, 'type' => 'varchar'),
            'author' => array('constraint' => 11, 'type' => 'int'),
            'section' => array('type' => 'smallint'),
            'rating' => array('type' => 'smallint'),
            'views' => array('type' => 'bigint'),
            'attached' => array('type' => 'bool'),
            'closed' => array('type' => 'bool'),
            'answer' => array('type' => 'smallint'),
            'created_at' => array('constraint' => 11, 'type' => 'int'),
            'updated_at' => array('constraint' => 11, 'type' => 'int'),
            'created_at' => array('constraint' => 11, 'type' => 'int'),
            'updated_at' => array('constraint' => 11, 'type' => 'int'),

        ), array('id'));

        \DBUtil::create_index('threads', 'author');
        \DBUtil::create_index('threads', 'section');
        \DBUtil::create_index('threads', 'attached');
        \DBUtil::create_index('threads', 'updated_at');

    }

    public function down()
    {
        \DBUtil::drop_table('threads');
    }
}