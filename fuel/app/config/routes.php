<?php
return array(
	//'_root_'  => 'welcome/index',  // The default route,
    '_root_'  => 'forum/index',  // The default route
	'_404_'   => 'welcome/404',    // The main 404 route

    'auth' => 'forum/auth',
    'registration' => 'forum/registration',

	'forum' => 'forum/index',
    'forum/sections' => 'forum/sections',
    'forum/section(/:id)/page(/:page)' => 'forum/section',
    'forum/last' => 'forum/last',
    'forum/thread(/:id)/page(/:page)' => 'forum/thread',

    'forum/user/:username' => 'forum/profile',

    'forum/thread/add' => 'forum/thread',
    'forum/answer/add' => 'forum/answer',

	'hello(/:name)?' => array('welcome/hello', 'name' => 'hello'),


);