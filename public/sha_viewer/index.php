<html>
<head>
	<title>test</title>
	<script type="text/javascript" src="/assets/js/0.9.15.min.js"></script>

</head>
<body>
	    <textarea name="sha" id="sha_viewer" cols="30" rows="10" style="width: 100%;display: none">
        Make(delphi)
        Add(MainForm,6560,21,105)
        {
        Left=21
        Top=105
        Font=[Arial,8,0,0,1]
        }
        Add(Edit,92657,196,56)
        {
        Left=80
        Top=5
        Width=305
        Height=21
        Font=[Arial,8,0,0,1]
        }
        Add(HubEx,14810186,175,154)
        {
        link(onEvent,23272:doAdd,[])
        }
        Add(Button,97963,70,105)
        {
        Left=10
        Top=10
        Font=[Arial,8,0,0,1]
        Caption="Red"
        Data=Integer(255)
        link(onClick,73391:doEvent1,[])
        }
        Add(RichEdit,23272,196,161)
        {
        Left=80
        Top=35
        Width=305
        Height=235
        Font=[Arial,8,0,0,1]
        ScrollBars=3
        link(Str,92657:Text,[])
        link(Color,4665:Value,[])
        link(Style,13914:Value,[(216,146)(258,146)])
        }
        Add(Hub,64747,126,154)
        {
        link(onEvent1,6777491:doWork3,[(172,160)])
        link(onEvent2,14810186:doWork2,[])
        }
        Add(HubEx,6777491,168,98)
        {
        link(onEvent,4665:doValue,[])
        }
        Add(Memory,13914,252,105)
        {
        Default=Integer(3)
        }
        Add(Button,52741,70,154)
        {
        Left=10
        Top=35
        Font=[Arial,8,0,0,1]
        Caption="Blue"
        Data=Integer(15178569)
        link(onClick,64747:doEvent1,[])
        }
        Add(Hub,73391,126,105)
        {
        link(onEvent1,6777491:doWork2,[])
        link(onEvent2,14810186:doWork1,[(179,118)])
        }
        Add(Memory,4665,203,105)
        {
        }
        Add(Button,70644,70,189)
        {
        Left=10
        Top=70
        Font=[Arial,8,0,0,1]
        Caption="Save"
        Data=String(d:\\t1.rtf)
        link(onClick,23272:doSave,[])
        }
    </textarea>
    <table class="table table-bordered pull-right" style="width: 200px">
        <thead>
            <tr>
                <td>Name</td><td>Value</td>
            </tr>
        </thead>
        <tbody id="props">
        </tbody>
    </table>
	<canvas id="canvas" width="500" height="500" style="height:100%;width: 100%;" />
	<script type="text/javascript" src="http://yandex.st/jquery/1.8.3/jquery.min.js"></script>
    <link type="text/css" id="theme_css" rel="stylesheet" href="/assets/css/themes/bootstrap.white.min.css" />  
    <script type="text/javascript">
UTF8 = {
    encode: function(s){
        for(var c, i = -1, l = (s = s.split("")).length, o = String.fromCharCode; ++i < l;
            s[i] = (c = s[i].charCodeAt(0)) >= 127 ? o(0xc0 | (c >>> 6)) + o(0x80 | (c & 0x3f)) : s[i]
        );
        return s.join("");
    },
    decode: function(s){
        for(var a, b, i = -1, l = (s = s.split("")).length, o = String.fromCharCode, c = "charCodeAt"; ++i < l;
            ((a = s[i][c](0)) & 0x80) &&
            (s[i] = (a & 0xfc) == 0xc0 && ((b = s[i + 1][c](0)) & 0xc0) == 0x80 ?
            o(((a & 0x03) << 6) + (b & 0x3f)) : o(128), s[++i] = "")
        );
        return s.join("");
    }
};

    function parseINIString(data){
    var regex = {
        section: /^\s*\[\s*([^\]]*)\s*\]\s*$/,
        param: /^\s*([\+\*@\w\.\-\_]+)\s*=\s*(.*?)\s*$/,
        comment: /^\s*;.*$/
    };
    var value = {};
    var lines = data.split(/\r\n|\r|\n/);
    var section = null;
    lines.forEach(function(line){
        if(regex.comment.test(line)){
            return;
        }else if(regex.param.test(line)){
            var match = line.match(regex.param);
            if(section){
                value[section][match[1]] = match[2];
            }else{
                value[match[1]] = match[2];
            }
        }else if(regex.section.test(line)){
            var match = line.match(regex.section);
            value[match[1]] = {};
            section = match[1];
        }else if(line.length == 0 && section){
            section = null;
        };
    });
    return value;
}
        var elements_sorted = [];
        $(function(){
            var sha = $('textarea#sha_viewer').val();


            // Parse sha
                var elements = [];

                var selection = false;
                var quote = false;
                var element = false;
                var element_start = false;
                var params_start = false;
                var make_start = false;
                var make = false;
                for(var i in sha){
                    if(!make && !selection && sha.substr(i, 4) == 'Make') {
                        make_start = true; 
                    } else
                    if(make_start && !selection && sha[i] == '(') {
                        make_start = i; 
                    } else
                    if(!make && make_start && sha[i] == ')') {
                        make = sha.substr(make_start, i - make_start).substr(1)
                    }

                    if(!selection && sha.substr(i, 3) == 'Add') {
                        selection = true; 
                    } else
                    if(selection && !element && !element_start && sha[i] == '(') {
                        element_start = i;
                    } else
                    if(selection &&  !element && element_start && sha[i] == ')') {
                       element = sha.substr(element_start, i - element_start).substr(1);
                    }else
                    if(selection && element && sha[i] == '{') {
                        params_start = i;
                    } else
                    if(selection && element && sha[i] == '"') {
                      quote = !quote;
                    } else
                    if(selection && element && !quote && sha[i] == '}') {
                        //console.log(sha.substr(params_start, i-params_start))
                        params = sha.substr(params_start, i-params_start).substr(1).split('\n');
                        var prop = [];
                        for(var j in params) {
                            param = params[j].trim('   \t\r').split('=', 2)
                            if(param[0])
                                prop.push({ name: param[0], value: param[1]})
                        }

                        elements.push({
                            element: element.split(","),
                            params: prop,
                        })

                        selection = false,quote = false,element = false,element_start = false,params_start = false;
                    }
                }


		

                //var canvas = new fabric.Canvas('canvas');
                var canvas = new fabric.Canvas('canvas', {
                  backgroundColor: 'white'
                });
                
                var HiElement = fabric.util.createClass(fabric.Object, fabric.Observable, {
                  H_PADDING: 20,
                  V_PADDING: 50,
                  initialize: function(src, options) {
                    this.callSuper('initialize', options);
                    this.image = new Image();
                    this.image.src = src;
                    this.image.onload = (function() {
                      this.width = this.image.width;
                      this.height = this.image.height;
                      this.id = options.id;
                      this.dots = options.dots;
                      //this.pleft = options.pleft;
                      //this.ptop = options.ptop;
                      this.loaded = true;
                      this.setCoords();
                      this.fire('image:loaded');
                    }).bind(this);
                  },
                  _render: function(ctx) {
                    if (this.loaded) {
                      ctx.fillStyle = '#999';
                      ctx.fillRect(
                        this.width - 40,
                        this.height - 40 ,
                        33,
                        33);
                      ctx.drawImage(this.image, -this.width / 2, -this.height / 2);
                    }
                  }
                });

                canvas.observe('object:selected', function(e) {
                    var element = elements_sorted[e.target.id];
                    var props = $('tbody#props')
                    $('tbody#props > tr').remove()
                    for(var i in element.params) {
                        var el = element.params[i];
                        props.append('<tr><td>'+el.name+'</td><td>'+el.value+'</td></tr>');
                    }
                });
                canvas.on('object:moving', function(e) {
                      var p = e.target;
                      for(var i in p.dots) {
                        var params = p.dots[i];
                        params.dot.set({ 'left': p.left + params.pleft, 'top': p.top + params.ptop });
                      }
                     // p.dot != 0 && p.dot.set({ 'left': p.left + p.pleft, 'top': p.top + p.ptop });
                      //canvas.renderAll();
                });

                for(i in elements) {
                    var element = elements[i].element;
                    var params = elements[i].params;
                    var name = element[0];
                    var src   = (make ? make : 'delphi') + '/icon/' + name + '.ico';
                    var size  = name == 'Hub' ? 15: (name == 'HubEx' ? 5: 30);
                    var left = element[2] * 1;
                    var top = element[3] * 1;
                    var id = element[1];
                    var dot = 0;
					elements_sorted[id] = {name: name, id: id, params: params, dots: {}};

                    var result;
                    $.ajax({url: '/sha_viewer/delphi/conf/'+name+'.ini', async: false}).success(function(data) {
                        result = parseINIString(data)
                    })

                    var i = 0;
                    var k = 0;
                    var dots = []
                    for(var j in result.Methods) {

                        if(j[0] == 'o' || j[0] == 'd') {
                            
                            if( size == 30) {

                                var _left = (j[0] == 'd') ? -18 : 18;
                                var pos = (j[0] == 'd') ? i : k;

                                if(pos > 7 * 3)
                                    break;
                                
                                dot = new fabric.Circle({
                                   left: left + _left,
                                   top: top - 10 + pos,
                                   radius: 2,
                                   fill: '#0b0'
                                 });

                                dot.selectable = false;
                                elements_sorted[id].dots[j] = dot;
                                dots.push({dot:dot, pleft: 18, ptop: pos-10 })
                                canvas.add(dot);
                                
                                if(j[0] == 'd')
                                    i+=7;
                                else
                                    k+=7;
                            }
                        }

                    }
                    
                    
                    for(var j in params) {
                        var param = params[j].name;
                       if(param.substr(0, 4) == 'link') {
                            param = param.substr(5, param.length - 6);
                            param = param.split(',');
                            var start = param[0];
                            var stop = param[1].split(':')[0];
                            console.log(stop)
                        }
                    }
                     

                    var hiEl = new HiElement(src, {
                      top: top,
                      left: left,
                      id: id,
                      dots: dots,
                      width: size,
                      height: size
                    });

                    hiEl.hasControls = false;

					hiEl.on('image:loaded', canvas.renderAll.bind(canvas));
                    
					canvas.add(hiEl);
                    

                }
            canvas.renderAll();

        })
    </script>
</body>
</html>