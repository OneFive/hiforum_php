<html>
<head>
    <title></title>
</head>
<body>
 sha: rich edit exmple <br>
    <textarea name="sha" id="sha_viewer" cols="30" rows="10" style="width: 100%;display: none">
        Make(delphi)
        Add(MainForm,6560,21,105)
        {
        Left=21
        Top=105
        Font=[Arial,8,0,0,1]
        }
        Add(Edit,92657,196,56)
        {
        Left=80
        Top=5
        Width=305
        Height=21
        Font=[Arial,8,0,0,1]
        }
        Add(HubEx,14810186,175,154)
        {
        link(onEvent,23272:doAdd,[])
        }
        Add(Button,97963,70,105)
        {
        Left=10
        Top=10
        Font=[Arial,8,0,0,1]
        Caption="Red"
        Data=Integer(255)
        link(onClick,73391:doEvent1,[])
        }
        Add(RichEdit,23272,196,161)
        {
        Left=80
        Top=35
        Width=305
        Height=235
        Font=[Arial,8,0,0,1]
        ScrollBars=3
        link(Str,92657:Text,[])
        link(Color,4665:Value,[])
        link(Style,13914:Value,[(216,146)(258,146)])
        }
        Add(Hub,64747,126,154)
        {
        link(onEvent1,6777491:doWork3,[(172,160)])
        link(onEvent2,14810186:doWork2,[])
        }
        Add(HubEx,6777491,168,98)
        {
        link(onEvent,4665:doValue,[])
        }
        Add(Memory,13914,252,105)
        {
        Default=Integer(3)
        }
        Add(Button,52741,70,154)
        {
        Left=10
        Top=35
        Font=[Arial,8,0,0,1]
        Caption="Blue"
        Data=Integer(15178569)
        link(onClick,64747:doEvent1,[])
        }
        Add(Hub,73391,126,105)
        {
        link(onEvent1,6777491:doWork2,[])
        link(onEvent2,14810186:doWork1,[(179,118)])
        }
        Add(Memory,4665,203,105)
        {
        }
        Add(Button,70644,70,189)
        {
        Left=10
        Top=70
        Font=[Arial,8,0,0,1]
        Caption="Save"
        Data=String(d:\\t1.rtf)
        link(onClick,23272:doSave,[])
        }
    </textarea>
    <style type="text/css">
        .element {
            position: fixed;
            top: 0px;
            left: 0px;
            border: solid 1px black;
            width: 30px;
            height: 30px;
        }
        .param {
            width: 100%;
        }
        .dot {
            position: fixed;
            height: 5px;
            width: 5px;
            background-color: #00aa00;
            border-radius: 2px;
            z-index: 99999;
        }
    </style>

    <div class="sha_viewer" style="height: 100%;">
        <span id="params" style="float: right;width: 200px;height: 100%;background-color: gray">

        </span>
    </div>

    <script type="text/javascript" src="http://yandex.st/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript">
    function parseINIString(data){
    var regex = {
        section: /^\s*\[\s*([^\]]*)\s*\]\s*$/,
        param: /^\s*([\+\*@\w\.\-\_]+)\s*=\s*(.*?)\s*$/,
        comment: /^\s*;.*$/
    };
    var value = {};
    var lines = data.split(/\r\n|\r|\n/);
    var section = null;
    lines.forEach(function(line){
        if(regex.comment.test(line)){
            return;
        }else if(regex.param.test(line)){
            var match = line.match(regex.param);
            if(section){
                value[section][match[1]] = match[2];
            }else{
                value[match[1]] = match[2];
            }
        }else if(regex.section.test(line)){
            var match = line.match(regex.section);
            value[match[1]] = {};
            section = match[1];
        }else if(line.length == 0 && section){
            section = null;
        };
    });
    return value;
}
        var elements_sorted = [];
        $(function(){
            var sha = $('textarea#sha_viewer').val();


            // Parse sha
                var elements = [];

                var selection = false;
                var quote = false;
                var element = false;
                var element_start = false;
                var params_start = false;
                var make_start = false;
                var make = false;
                for(var i in sha){
                    if(!make && !selection && sha.substr(i, 4) == 'Make') {
                        make_start = true; 
                    } else
                    if(make_start && !selection && sha[i] == '(') {
                        make_start = i; 
                    } else
                    if(!make && make_start && sha[i] == ')') {
                        make = sha.substr(make_start, i - make_start).substr(1)
                    }

                    if(!selection && sha.substr(i, 3) == 'Add') {
                        selection = true; 
                    } else
                    if(selection && !element && !element_start && sha[i] == '(') {
                        element_start = i;
                    } else
                    if(selection &&  !element && element_start && sha[i] == ')') {
                       element = sha.substr(element_start, i - element_start).substr(1);
                    }else
                    if(selection && element && sha[i] == '{') {
                        params_start = i;
                    } else
                    if(selection && element && sha[i] == '"') {
                      quote = !quote;
                    } else
                    if(selection && element && !quote && sha[i] == '}') {
                        //console.log(sha.substr(params_start, i-params_start))
                        params = sha.substr(params_start, i-params_start).substr(1).split('\n');
                        var prop = [];
                        for(var j in params) {
                            param = params[j].trim('   \t\r').split('=', 2)
                            if(param[0])
                                prop.push({ name: param[0], value: param[1]})
                        }

                        elements.push({
                            element: element.split(","),
                            params: prop,
                        })

                        selection = false,quote = false,element = false,element_start = false,params_start = false;
                    }
                }


                var sha_viewer = $('.sha_viewer');
                var props_div = $('#params');

                for(i in elements) {
                    var element = elements[i].element;
                    var params = elements[i].params;
                    var src = (make ? make : 'delphi') + '/icon/' + element[0];
                    var left = element[2];
                    var top = element[3];

                    
                    var css = element[0] == 'Hub' || element[0] == 'HubEx' ? 'width: 15px;height:15px;': ''
                    sha_viewer.append('<img src="/sha_viewer/'+ src +'.ico" id="'+ element[1] +'" style="left:' +left+ 'px;top:'+top+'px;'+css+'" class="element"/>')
                    
                    $.ajax({url: '/sha_viewer/delphi/conf/Memory.ini', async: false}).success(function(data) {
                        result = parseINIString(data)
                    })

                    sha_viewer.append('<div class="dot" style="left:' +(left*1+(css?15:30))+ 'px;top:'+(top*1+5)+'px;"></div>')
                    
                    elements_sorted[element[1]] = {name: element[0], id: element[1], params: params, ini: result}
                    
                    
                    


                    //for(j in params)
                     //   props_div.append('<div class="param">'+ params[j].name +' = '+ params[j].value +'</div>')
                }

                $('.element').click(function(){
                    var id = $(this).attr('id');
                    $('#params > div').remove();
                    for(j in elements_sorted[id].params) {
                       var element = elements_sorted[id].params[j];
                       props_div.append('<div class="param"><b>'+ element.name +'</b> : <input type="text" style="width:100px" value="'+ element.value +'"></div>')
                   }
                })
            //

        })
    </script>
</body>
</html>
