-- phpMyAdmin SQL Dump
-- version 3.5.3
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Дек 01 2012 г., 11:49
-- Версия сервера: 5.5.28-log
-- Версия PHP: 5.4.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `test`
--

-- --------------------------------------------------------

--
-- Структура таблицы `sections`
--

CREATE TABLE IF NOT EXISTS `sections` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(150) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `parent` smallint(6) DEFAULT NULL,
  `sort` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=149 ;

--
-- Дамп данных таблицы `sections`
--

INSERT INTO `sections` (`id`, `name`, `description`, `icon`, `parent`, `sort`) VALUES
(110, 'Help', 'Помощь в использование пакета', NULL, 106, 8),
(109, 'Ошибки в компонентах', 'Сообщения о найденных ошибках, недоделках и опечатках', NULL, 106, 7),
(111, 'Прочее', 'Все остальные вопросы по теме пакета', NULL, 106, 9),
(112, 'HiAsm PocketPC', NULL, 'hp_ipaq_111.png', 0, 10),
(113, 'Среда Micro HiAsm', 'Сообщения об ошибках\\дополнениях в среде', NULL, 112, 11),
(114, 'Делаем компоненты', 'Раздел для разработчиков компонент', NULL, 112, 12),
(115, 'Ошибки в компонентах', 'Сообщения о найденных ошибках, недоделках и опечатках', NULL, 112, 13),
(116, 'Help', 'Помощь в использование пакета и среды', NULL, 112, 14),
(117, 'Прочее', 'Все остальные вопросы по теме', NULL, 112, 15),
(118, 'Пакет WEB', NULL, 'pre_web1.png', 0, 16),
(119, 'Делаем компоненты', 'Раздел для разработчиков компонент', NULL, 118, 17),
(120, 'Ошибки в компонентах', 'Сообщения о найденных ошибках, недоделках и опечатках', NULL, 118, 18),
(121, 'Help', 'Помощь в использование пакета', NULL, 118, 19),
(122, 'Прочее', 'Все остальные вопросы по теме пакета', NULL, 118, 20),
(123, 'Среда HiAsm', NULL, 'hiasm.png', 0, 21),
(124, 'Ошибки в среде', 'Здесь пишите о найденных ошибках в программе', NULL, 123, 22),
(125, 'Задачи', 'Список наиболее интересных пожеланий', NULL, 123, 23),
(126, 'Help', 'Помощь в использование среды и работе в ней', NULL, 123, 24),
(127, 'HiAsm 5', 'HiAsm Studio v5', NULL, 123, 25),
(128, 'Hion', 'HiAsm Online', NULL, 123, 26),
(129, 'Прочие пакеты', NULL, 'other.png', 0, 27),
(130, 'Пакет QT & wxWidget', 'Обсуждение и решение проблем в использовании пакетов QT & wxWidget', NULL, 129, 28),
(131, 'Пакет FASM', 'Доработки и улучшения пакета FASM', NULL, 129, 29),
(132, 'Пакет VBS', 'Обсуждение и решение проблем в использовании пакета VBS', NULL, 129, 30),
(133, 'Прочие пакеты', 'Все остальные вопросы по теме пакетов', NULL, 129, 31),
(134, 'Треп', NULL, 'users.png', 0, 32),
(135, '"Игра в слова"', 'Тут можно говорить о всем, чем угодно', NULL, 134, 33),
(136, 'Между делом', 'Обсуждение проблем и задач при написание программ', NULL, 134, 34),
(137, 'КуплюПродам', 'Купля/продажа интеллектуального труда', NULL, 134, 35),
(138, 'Дизайн', 'Раздел для художников и креативщиков', NULL, 134, 36),
(139, 'Соревнования', 'Состязания в знании и умении работать с hiasm', NULL, 134, 37),
(140, 'Minecraft', 'Игра для программистов', NULL, 134, 38),
(141, 'Файловый архив', NULL, 'normal_folder.png', 0, 39),
(142, 'HiAsm', 'Новые версии HiAsm', NULL, 141, 40),
(143, 'Схемы', 'Исходные схемы программ, написанных в HiAsm', NULL, 141, 41),
(144, 'Компоненты', 'Компоненты для HiAsm', NULL, 141, 42),
(145, 'Видео', 'Видеоуроки по работе в среде HiAsm', NULL, 141, 43),
(146, 'Плагины', 'Плагины для форума', NULL, 141, 44),
(147, 'Тестовый раздел', NULL, 'preferences_system.png', 0, 45),
(148, 'Раздел', '', NULL, 147, 46),
(108, 'Новые компоненты', 'Оставляйте свои идеи по добавлению в пакет новых компонент', NULL, 106, 6),
(107, 'Делаем компоненты', 'Раздел для разработчиков компонент', NULL, 106, 5),
(106, 'Пакет Windows', NULL, 'Windows-icon.png', 0, 4),
(105, 'Поддержка', 'Раздел поддержки online ресурсов портала', NULL, 103, 3),
(104, 'Информация', 'Информация для посетителей форума и пользователей конструктора', NULL, 103, 2),
(103, 'Администрация', NULL, 'help.png', 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `threads`
--

CREATE TABLE IF NOT EXISTS `threads` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) DEFAULT NULL,
  `author` int(10) unsigned DEFAULT NULL,
  `section` smallint(5) unsigned NOT NULL,
  `rating` smallint(6) DEFAULT NULL,
  `views` bigint(20) unsigned DEFAULT NULL,
  `attached` tinyint(1) DEFAULT NULL,
  `closed` tinyint(1) DEFAULT NULL,
  `answer` smallint(6) DEFAULT NULL,
  `created_at` int(10) unsigned DEFAULT NULL,
  `updated_at` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `author` (`author`),
  KEY `section` (`section`),
  KEY `attached` (`attached`),
  KEY `updated_at` (`updated_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
